import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import { Post } from 'src/app/interfaces/interfaces';
import { AlertController } from '@ionic/angular';




@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})

export class InicioPage implements OnInit {
  
  servicioPost: Post;

  constructor(public _service: PostService, public  alertController: AlertController) { }
 
  componentes: Componente[] = [
    {
      icon: 'american-football',
      name: 'Action Sheet',
      redirectTo: '/action-sheet'
    },
    {
      icon: 'appstore',
      name: 'Alert',
      redirectTo: '/alert'
    }
  ];

  ngOnInit() {
    //this._service.ejecutarQuery();
    this.cargarPost();
    //console.log(this.servicioPost);
  }
  
cargarPost(){
      this._service.Post()
      .subscribe( (resp?) => {
        if(resp){
          this.servicioPost = resp;
        }
      }
      );
      console.log(this.servicioPost);
  }

  updatePost(id){
    console.log('Update');
  }

  eliminarPost(id){
    console.log('Eliminado Post ',id);
   this._service.Delete(id);
  }

  deletePost(){
    console.log('Fuction Delete');
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Post Eliminado',
      subHeader: 'Delete',
      message: 'Se elimino el post correctamente ',
      buttons: ['OK']
    });

    await alert.present();

}


async alertDelete(id) {
  const alert = await this.alertController.create({
    header: '!Esta seguro que desea eliminar?',
    subHeader: `Post Id: ${ id }`,
    message: '',
    buttons: [
      {
        text:'Cancelar',
        handler: () =>{
          console.log('Cancel');
        }
      },
      {
        text:'Borrar',
        handler: () =>{
          this.eliminarPost(id);
        }
      }
    ]
  });

  await alert.present();
}


}

interface Componente{
  icon: string;
  name: string;
  redirectTo: string;
}
