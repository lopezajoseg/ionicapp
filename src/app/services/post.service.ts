import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { enviroment } from '../enviroments/enviroment';
import { Post, Comentarios } from '../interfaces/interfaces';

const apiUrlFull: string = enviroment.apiUrlFull;
const apiUrlFullpostId: string = enviroment.apiUrlComenst;

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  private ejecutarQuery<T>( query: string ) {

    return this.http.get<T>( query);

  }

  public Post(){
    return this.ejecutarQuery<Post>(apiUrlFull);
  }

  public Delete(id){
    console.log(id);
    let urlDeletePost = `${apiUrlFull}/${ id }`;
    console.log(urlDeletePost);
    return this.http.put(urlDeletePost, resp => console.log('post eliminado '+ id));
  }

  public Coments(id: string){
    let urlComenID =`${ apiUrlFullpostId }postId=${ id }`;
    console.log(urlComenID);
    return this.ejecutarQuery<Comentarios>(urlComenID);
  }
}
