export interface Post{
    userId?: string;
    id?: string;
    title?: string;
    body?: string;
}

export interface Comentarios {
  postId?: string;
  id?: number;
  name?: string;
  email?: string;
  body?: string;
}