import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../../services/post.service';
import { Comentarios } from '../../interfaces/interfaces';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.page.html',
  styleUrls: ['./alert.page.scss'],
})
export class AlertPage implements OnInit {
  comentarios: Comentarios;
  
  constructor( private route: ActivatedRoute,
     public _service : PostService) { 
  }
  
  ngOnInit() {
    this.route.params
      .subscribe( parametros =>{
        this._service.Coments(parametros['id'])
        .subscribe( resp =>{
          console.log(resp);
          this.comentarios = resp;
        });
      });
  }

  

}
